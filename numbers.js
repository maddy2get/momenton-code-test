exports.MESSAGE = {
  BREAKFAST: 'Breakfast',
  LUNCH: 'Lunch',
  BRUNCH: 'Brunch'
}

exports.printNumbers = (start, end) => {
  for( number = start; number <= end; number++ ) {
    console.log(this.getOutput(number));
  }
}

exports.getOutput = (number) => {
  let output;
  
  if ( this.isBrunch(number) ) {
    output = this.MESSAGE.BRUNCH;
  } else if ( this.isBreakfast(number) ) {
    output = this.MESSAGE.BREAKFAST;
  } else if ( this.isLunch(number) ) {
    output = this.MESSAGE.LUNCH;
  } else {
    output = number;
  }

  return output;
}

exports.isBreakfast = (number) => {
  return number % 3 === 0;
}

exports.isLunch = (number) => {
  return number % 5 === 0;
}

exports.isBrunch = (number) => {
  return number % 3 === 0 && number % 5 === 0
}

this.printNumbers(50, 500);
