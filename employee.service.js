exports.getNestedData = (data, key, root = undefined) => {
  let nest = {};
  data.forEach(nestItem => {
    if ( nestItem.id ) {
      nest[nestItem.id] = {...nest[nestItem.id], ...nestItem};
      nest[nestItem[key]] = nest[nestItem[key]] || {};
      nest[nestItem[key]].children = nest[nestItem[key]].children || [];
      nest[nestItem[key]].children.push(nest[nestItem.id]);
    }
  });
  return nest[root].children;
}

exports.getTableData = (tree, position = 0, tableData = []) => {
  tree.forEach((row) => {
    const newRow = [];
    newRow[position] = row.employeeName;
    tableData.push(newRow);
    if (row.children) {
      this.getTableData(row.children, position+1, tableData);
    }
  });
  return this.formatTableData(tableData);
}

exports.formatTableData = (data) => {
  let maxLength = 0;
  data.forEach((row) => {
    if ( row.length > maxLength ) {
      maxLength = row.length;
    }
  });

  data = data.map(row => {
    row.length = maxLength;
    return Array.from(row, item => item || null);
  });

  return data;
}