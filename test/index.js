const expect = require('chai').expect;
const sinon = require('sinon');
const DisplayService = require('../index');

describe('Display Employees', () => {
  it('should display employees using console log', () => {
    let spy = sinon.spy(console, 'log');
    DisplayService.displayEmployees("employees");
    expect(spy.calledWith("employees")).to.equal(true);
    spy.restore();
  });
});

