const ceo = { employeeName: 'CEO', id: 1 };
const managerOne = { employeeName: 'mangerOne', id: 2, managerId: 1};
const employeeOne = { employeeName: 'employeeOne', id: 3, managerId: 2};
const employeeTwo = { employeeName: 'employeeTwo', id: 4, managerId: 2};
const managerTwo = { employeeName: 'mangerTwo', id: 5, managerId: 1};
const employeeThree = { employeeName: 'employeeThree', id: 6, managerId: 5};
const invalidEmployee = { employeeName: 'invalidEmployee', managerId: 5};
const invalidManager = { employeeName: 'invalidManager', managerId: 99};
const internEmployee = { employeeName: 'interEmployee', id: 7, managerId: 4};

exports.CEOEmployees = [ceo];
exports.employees = [ceo, managerOne, managerTwo, employeeOne, employeeTwo, employeeThree];
exports.internEmployees = [...this.employees, internEmployee]
exports.invalidEmployees = [...this.employees, invalidEmployee];
exports.invalidManagerEmployees = [...this.employees, invalidEmployee, invalidManager];

exports.expectedCEOEmployees = [ceo];

exports.expectedEmployees = [{
  ...ceo,
  children: [
    {
      ...managerOne,
      children: [
        employeeOne,
        employeeTwo
      ]
    },
    {
      ...managerTwo,
      children: [
        employeeThree
      ]
    }
  ]
}];

exports.expectedInternEmployees = [{
  ...ceo,
  children: [
    {
      ...managerOne,
      children: [
        employeeOne,
        {
          ...employeeTwo,
          children: [
            internEmployee
          ]
        }
      ]
    },
    {
      ...managerTwo,
      children: [
        employeeThree
      ]
    }
  ]
}];

exports.expectedTableData = [
  [ceo.employeeName, null, null],
  [null, managerOne.employeeName, null],
  [null, null, employeeOne.employeeName],
  [null, null, employeeTwo.employeeName],
  [null, managerTwo.employeeName, null],
  [null, null, employeeThree.employeeName]
];

exports.expectedInternTableData = [
  [ceo.employeeName, null, null, null],
  [null, managerOne.employeeName, null, null],
  [null, null, employeeOne.employeeName, null],
  [null, null, employeeTwo.employeeName, null],
  [null, null, null, internEmployee.employeeName],
  [null, managerTwo.employeeName, null, null],
  [null, null, employeeThree.employeeName, null]
];