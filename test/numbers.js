const expect = require('chai').expect;
const sinon = require('sinon');
const { printNumbers, isBreakfast, isLunch, isBrunch, getOutput, MESSAGE } = require("../numbers");

describe('Print Numbers - printNumbers', () => {
  it('should display numbers using console log', () => {
    let spy = sinon.spy(console, 'log');
    printNumbers(50, 500);
    expect(spy.called).to.equal(true);
    expect(spy.callCount).to.equal(451);
    spy.restore();
  });

  it('should return true if number is muptiple of 3', () => {
    expect(isBreakfast(3)).to.equal(true);
  });

  it('should return true if number is muptiple of 3', () => {
    expect(isBreakfast(3)).to.equal(true);
  });
});

describe('Print Numbers - getOutput', () => {
  it('should display breakfast if number is multiple of 3', () => {
    expect(getOutput(3)).to.equal(MESSAGE.BREAKFAST);
  });

  it('should display lunch if number is multiple of 5', () => {
    expect(getOutput(5)).to.equal(MESSAGE.LUNCH);
  });

  it('should display brunch if number is multiple of 3 and 5', () => {
    expect(getOutput(15)).to.equal(MESSAGE.BRUNCH);
  });

  it('should display number itself if number is not multiple of 3 or 5', () => {
    expect(getOutput(4)).to.equal(4);
  });
});

describe('Print Numbers - isBreakfast', () => {
  it('should return true if number is muptiple of 3', () => {
    expect(isBreakfast(3)).to.equal(true);
  });

  it('should return false if number is not muptiple of 3', () => {
    expect(isBreakfast(5)).to.equal(false);
  });
});

describe('Print Numbers - isLunch', () => {
  it('should return true if number is muptiple of 5', () => {
    expect(isLunch(5)).to.equal(true);
  });

  it('should return false if number is muptiple of 5', () => {
    expect(isLunch(3)).to.equal(false);
  });
});

describe('Print Numbers - isBrunch', () => {
  it('should return true if number is muptiple of 3 and 5', () => {
    expect(isBrunch(15)).to.equal(true);
  });

  it('should return false if number is not muptiple of 3 and 5', () => {
    expect(isBrunch(3)).to.equal(false);
    expect(isBrunch(5)).to.equal(false);
  });
});