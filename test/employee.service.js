const expect = require('chai').expect;
const EmployeeService = require('../employee.service');
const mocks = require('./mocks');

describe('EmpoyeeService - getNestedData', () => {
  it('should get nested employees when only ceo is available', () => {
    expect(EmployeeService.getNestedData(mocks.CEOEmployees, 'managerId')).to.deep.equal(mocks.expectedCEOEmployees);
  });

  it('should get nested employees', () => {
    expect(EmployeeService.getNestedData(mocks.employees, 'managerId')).to.deep.equal(mocks.expectedEmployees);
  });

  it('should get nested employees when invalid employee is available', () => {
    expect(EmployeeService.getNestedData(mocks.invalidEmployees, 'managerId')).to.deep.equal(mocks.expectedEmployees);
  });

  it('should get nested employees when invalid manager is provided', () => {
    expect(EmployeeService.getNestedData(mocks.invalidEmployees, 'managerId')).to.deep.equal(mocks.expectedEmployees);
  });

  it('should get nested employees when 4 level nested employees provided', () => {
    expect(EmployeeService.getNestedData(mocks.internEmployees, 'managerId')).to.deep.equal(mocks.expectedInternEmployees);
  });
});

describe('EmployeeService - getTableData', () => {
  it('should get table data for nested employees', () => {
    expect(EmployeeService.getTableData(mocks.expectedEmployees)).to.deep.equal(mocks.expectedTableData);
  });

  it('should get table data for 4 level nested employees', () => {
    expect(EmployeeService.getTableData(mocks.expectedInternEmployees)).to.deep.equal(mocks.expectedInternTableData);
  });
});
