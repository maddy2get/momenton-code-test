const data = require('./data.json');
const Table = require('table');
const EmployeeService = require('./employee.service');

const employees = EmployeeService.getNestedData(data, 'managerId');
const table = Table.table(EmployeeService.getTableData(employees));

exports.displayEmployees = (data) => {
  console.log(data);
}

this.displayEmployees(table);